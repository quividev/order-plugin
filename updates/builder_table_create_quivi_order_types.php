<?php namespace Quivi\Order\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviOrderTypes extends Migration
{
    public function up()
    {
        Schema::create('quivi_order_types', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 255);
            $table->string('descr', 1024);
            $table->string('code', 255);
            $table->integer('sign')->default(1);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_order_types');
    }
}
