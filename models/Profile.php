<?php

namespace Quivi\Order\Models;

use Exception;
use Illuminate\Support\Collection;
use Quivi\Profile\Models\Profile as ProfileBase;

class Profile extends ProfileBase
{
    public $hasMany = [
        'orders' => 'Quivi\Order\Models\Orders'
    ];
}