<?php namespace Quivi\Order;

use System\Classes\PluginBase;

use App;

//use Quivi\Order\Components\InvoicesList;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function register()
    {
        $this->registerConsoleCommand('Quivi.Order:Sync', 'Quivi\Order\Console\Sync');
        $this->registerConsoleCommand('Quivi.Order:Invoicing', 'Quivi\Order\Console\Invoicing');
    }
  
    public function boot(){
        if (getenv('LC_MONETARY')) {
            setlocale(LC_MONETARY, getenv('LC_MONETARY'));
        }
    }

}
